# Maintainer: Richard Acayan <mailingradian@gmail.com>
# Stable Linux kernel with patches for SDM670 devices
# Kernel config based on: arch/arm64/configs/{defconfig,sdm{670,845}.config}

_flavor="postmarketos-qcom-sdm670"
pkgname=linux-$_flavor
pkgver=5.19.14
pkgrel=0
pkgdesc="Mainline Kernel fork for SDM670 devices"
arch="aarch64"
_carch="arm64"
url="https://gitlab.com/sdm670-mainline/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native"
makedepends="bash bison findutils flex installkernel openssl-dev perl"

_config="config-$_flavor.$arch"
_tag="sdm670-v$pkgver"

# Source
source="
	https://gitlab.com/sdm670-mainline/linux/-/archive/$_tag/linux-$_tag.tar.gz
	$_config
"
builddir="$srcdir/linux-$_tag"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$arch" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor"
}

package() {
	mkdir -p "$pkgdir"/boot

	install -Dm644 "$builddir/arch/$_carch/boot/Image.gz" \
		"$pkgdir/boot/vmlinuz"

	make modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot/ \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/boot/dtbs
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}

sha512sums="
cae2d985d2a6a993365da34d69f2428491f4034c8e2a3c2c00a1cc6392d0efd9b379187a385f1b629e9ef5ceaf5dd77931e72acd275d9e7fb8238ed100ca76fa  linux-sdm670-v5.19.14.tar.gz
9cf5910076d45a238375a2684557e66e169097be409a6fa9d9b018900a4f81bc5f9b998c8c855facad200a21bdca26ca6db4b4c53d705ac6a2f62b5f3311aa08  config-postmarketos-qcom-sdm670.aarch64
"
